﻿using UnityEngine;
using System.Collections;

public class EndArea : MonoBehaviour
{
    public bool isGoal = false;
    public GameObject currentCustomer;
	public Sliders scoreScreen;

    void Start()
    {
        currentCustomer = GameState.currentCustomer;
    }
	void OnTriggerEnter2D (Collider2D playerCheck)
    {
		PlayerController pc = playerCheck.GetComponent<PlayerController> ();
        if (pc != null)
        {
            if (isGoal)
            {
				MugContents mc = playerCheck.GetComponent<MugContents> ();
                Customer cust = currentCustomer.GetComponent<Customer>();
                cust.calculateScore(mc);
                scoreScreen.initSliders(mc, cust);
				scoreScreen.transform.parent.gameObject.SetActive(true);
            }
            pc.Respawn();
        }
        if (playerCheck.GetComponent<FlavorItem>())
            Destroy(playerCheck.gameObject);
	}
}
