﻿using UnityEngine;
using System.Collections;

public class DestroyButtonOnMobile : MonoBehaviour {

	// Use this for initialization
	void Awake () {
#if UNITY_IOS || UNITY_ANDROID
		Destroy(gameObject);
#endif
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
