﻿using UnityEngine;
using System.Collections;

public class GameState
{
    public static int score = 0;
    public static int customersServed = 0;
    public static bool pause = false;
	public static GameObject currentCustomer;
    public static int difficulty = 0;
    static int incrementDiffThreshold = 10;
    public static Texture2D pauseImage;
    
    public static void reset()
    {
        score = 0;
        customersServed = 0;
    }
    void OnGUI()
    {
        if (pause)
            GUI.Label(new Rect(0, 0, Screen.width, Screen.height), pauseImage);
    }
    public static void AddCustomerScore(int valueToAdd)
    {
        score += valueToAdd;
        if (score < incrementDiffThreshold) 
            difficulty++;
        customersServed++;
    }
}
