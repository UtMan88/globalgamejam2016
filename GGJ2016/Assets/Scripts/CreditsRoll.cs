﻿using UnityEngine;
using System.Collections;

public class CreditsRoll : MonoBehaviour
{
    public string teamName;
    public string gameTitle;
    public string[] design;
    public string[] programming;
    public string[] art;
    public string[] sound;
    public float scrollSpeed = 0;
    float y;
    string credits = "";

    void Start()
    {
        y = Screen.height;

        credits += teamName + " is:\n";
        credits += "\n\nDesign\n";
        for (int i = 0; i < design.Length; i++)
            credits += "\n\t" + design[i];
        credits += "\n\nProgramming\n";
        for (int i = 0; i < programming.Length; i++)
            credits += "\n\t" + programming[i];
        credits += "\n\nArt\n";
        for (int i = 0; i < art.Length; i++)
            credits += "\n\t" + art[i];
        credits += "\n\nAudio\n";
        for (int i = 0; i < sound.Length; i++)
            credits += "\n\t" + sound[i];

        credits += "\n\nThanks for playing " + gameTitle +"! \n-" + teamName;
    }
    void Update()
    {
        y -= scrollSpeed * Time.deltaTime;
    }
    void OnGUI()
    {
        GUI.Label(new Rect(Screen.width/2, y, Screen.width, Screen.height), credits);
    }
}
