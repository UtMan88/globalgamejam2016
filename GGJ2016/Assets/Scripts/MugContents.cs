﻿using UnityEngine;
using System.Collections;

public class MugContents : MonoBehaviour
{
    public int savoryValue = 0;
    public int fishValue = 0;
    public int meatyValue = 0;
    public int bitterValue = 0;
    public int nuttyValue = 0;
    public int fruityValue = 0;
    public int sweetValue = 0;
    public int saltyValue = 0;
    public int sourValue = 0;
    public int floweryValue = 0;
    public int cleanValue = 0;
    public int spicyValue = 0;
    public int creamyValue = 0;
	public int aromaticValue = 0;
	public int horribleValue = 0;

    public Flavor[] flavors;
    int maxFlavors = 10;
    int flavorCount = 0;
    void Start()
    {
        flavors = new Flavor[maxFlavors];
    }
    public void addIngredients (FlavorItem newFlavor)
    {

        if (flavorCount < maxFlavors)
        {
            bool duplicateFound = false;
            for (int i = 0; i < flavors.Length; i++) {
                if (newFlavor.flavor == flavors[i]) {
                    duplicateFound = true;
                }
            }
            if (!duplicateFound) {
                flavors[flavorCount] = newFlavor.flavor;
            }
            flavorCount++;
			Debug.Log ("FLAVOR TOWN");
            savoryValue += newFlavor.savory;
            fishValue += newFlavor.fish;
            meatyValue += newFlavor.meaty;
            bitterValue += newFlavor.bitter;
            nuttyValue += newFlavor.nutty;
            fruityValue += newFlavor.fruity;
            sweetValue += newFlavor.sweet;
            saltyValue += newFlavor.salty;
            sourValue += newFlavor.sour;
            floweryValue += newFlavor.flowery;
            cleanValue += newFlavor.clean;
            spicyValue += newFlavor.spicy;
            creamyValue += newFlavor.creamy;
			horribleValue += newFlavor.horrible;
			aromaticValue += newFlavor.aromatic; 
        }
    }

    public string ToCoffeeString()
    {
        string prefix = "";
        string coffeeType = "Coffee";
        string suffix = "";
        bool afterOne = false;
        foreach(Flavor flav in flavors)
        {
			if (flav == Flavor.NONE)
				continue;
            if(afterOne)
            {
                prefix += " ";
            }
            else
            {
                afterOne = true;
            }
            switch(flav)
            {
                case Flavor.LEATHER:
                    int randomLeather = Random.Range(0, 3);
                    switch(randomLeather)
                    {
                        case 0:
                            prefix += "Hardy";
                            break;
                        case 1:
                            prefix += "Chewy";
                            break;
                        case 2:
                            prefix += "Stretchy";
                            break;
                    }
                    //coffee += "Hardy";
                    break;
                case Flavor.LAVENDER:
                    int randomLavender = Random.Range(0, 3);
                    switch (randomLavender)
                    {
                        case 0:
                            prefix += "Passionate";
                            break;
                        case 1:
                            prefix += "Lavender";
                            break;
                        case 2:
                            prefix += "Vibrant";
                            break;
                    }
                    break;
                case Flavor.HONEY:
                    prefix += "Honey";
                    break;
                case Flavor.LEMON:
                    int randomLemon = Random.Range(0, 2);
                    switch(randomLemon)
                    {
                        case 0:
                            prefix += "Lemon";
                            break;
                        case 1:
                            prefix += "Sour";
                            break;
                    }
                    //coffee += "Lemon";
                    break;
                case Flavor.PEPPER:
                    int randomPepper = Random.Range(0, 2);
                    switch (randomPepper)
                    {
                        case 0:
                            prefix += "Peppercorn";
                            break;
                        case 1:
                            prefix += "Peppery";
                            break;
                    }
                    break;
                case Flavor.JALAPENO:
                    prefix += "Mexican";
                    break;
                case Flavor.BEES:
                    int randomBees = Random.Range(0, 2);
                    switch(randomBees)
                    {
                        case 0:
                            prefix += "Buzzy";
                            break;
                        case 1:
                            prefix += "Fuzzy";
                            break;
                    }
                    //coffee += "Buzzy";
                    break;
                case Flavor.PINE:
                    prefix += "Mountain Forest";
                    break;
                case Flavor.CHOCOLATE:
                    int randomChocolate = Random.Range(0, 2);
                    switch(randomChocolate)
                    {
                        case 0:
                            prefix += "Mocha";
                            break;
                        case 1:
                            prefix += "Chocolatey";
                            break;
                    }
                    //coffee += "Mocha";
                    break;
                case Flavor.BANANA:
                    int randomBanana = Random.Range(0, 3);
                    switch(randomBanana)
                    {
                        case 0:
                            prefix += "Hawaiian";
                            break;
                        case 1:
                            prefix += "African";
                            break;
                        case 2:
                            prefix += "Monkey Food";
                            break;
                    }
                    //coffee += "Hawaiian";
                    break;
                case Flavor.RAISINS:
                    prefix += "Grandma's";
                    break;
                case Flavor.VANILLA:
                    int randomVanilla = Random.Range(0, 3);
                    switch (randomVanilla)
                    {
                        case 0:
                            prefix += "Vanilla";
                            break;
                        case 1:
                            suffix += " with Vanilla Notes";
                            break;
                        case 2:
                            prefix += "Mild";
                            break;
                    }
                    break;
                case Flavor.CINNAMON:
                    prefix += "Cinnamon";
                    break;
                case Flavor.SOAP:
                    int randomSoap = Random.Range(0, 3);
                    switch(randomSoap)
                    {
                        case 0:
                            prefix += "Cleanly";
                            break;
                        case 1:
                            prefix += "Soapy";
                            break;
                        case 2:
                            prefix += "Colon Cleanser";
                            break;
                    }
                    //coffee += "Cleanly";
                    break;
                case Flavor.CREAM:
                    int randomCream = Random.Range(0, 3);
                    switch(randomCream)
                    {
                        case 0:
                            prefix += "Creme da la";
                            break;
                        case 1:
                            prefix += "Creamy";
                            break;
                        case 2:
                            coffeeType = "Latte";
                            break;
                        case 3:
                            prefix += "Soft";
                            break;
                    }
                    //coffee += "Creme da la";
                    break;
                case Flavor.TANGERINE:
                    prefix += "Citrus";
                    break;
                case Flavor.PEANUT:
                    prefix += "Nut";
                    break;
                case Flavor.ALMONDS:
                    prefix += "Almond";
                    break;
                case Flavor.HAZELNUTS:
                    prefix += "Hazelnut";
                    break;
                case Flavor.CARAMEL:
                    prefix += "Caramel";
                    break;
                case Flavor.SANDALS:
                    int randomSandals = Random.Range(0, 3);
                    switch(randomSandals)
                    {
                        case 0:
                            prefix += "Earthy";
                            break;
                        case 1:
                            prefix += "Footy";
                            break;
                        case 2:
                            suffix += " with Earthy Overtones";
                            break;
                    }
                    //coffee += "Earthy";
                    break;
                case Flavor.COCONUT:
                    int randomCoconut = Random.Range(0, 2);
                    switch (randomCoconut)
                    {
                        case 0:
                            prefix += "Island Nut";
                            break;
                        case 1:
                            prefix += "Coconut";
                            break;
                    }
                    break;
                case Flavor.HONEYDEW:
                    int randomHoneydew = Random.Range(0, 2);
                    switch(randomHoneydew)
                    {
                        case 0:
                            prefix += "Sweet Dew";
                            break;
                        case 1:
                            prefix += "Melon";
                            break;
                    }
                    //coffee += "Sweet Dew";
                    break;
                case Flavor.MINT:
                    prefix += "Mint";
                    break;
                case Flavor.PUMPKIN:
                    prefix += "Pumpkin Spice";
                    break;
                case Flavor.STRAWBERRY:
                    prefix += "Strawberry";
                    break;
                case Flavor.TOFFEE:
                    prefix += "English Toffee";
                    break;
                case Flavor.ROSES:
                    prefix += "Rosey";
                    break;
                case Flavor.JASMINE:
                    prefix += "Jasmine";
                    break;
                case Flavor.CHERRIES:
                    prefix += "Cherry";
                    break;
                case Flavor.FISH:
                    int randomFish = Random.Range(0, 3);
                    switch(randomFish)
                    {
                        case 0:
                            prefix += "Atlantic";
                            break;
                        case 1:
                            prefix += "Sushi";
                            break;
                        case 2:
                            prefix += "Chirashi";
                            break;
                    }
                    //coffee += "Atlantic";
                    break;
                case Flavor.SIRRACHA:
                    int randomSirracha = Random.Range(0, 2);
                    switch(randomSirracha)
                    {
                        case 0:
                            prefix += "Thai Spice";
                            break;
                        case 1:
                            prefix += "Rooster Spice";
                            break;
                    }
                    //coffee += "Thai Spice";
                    break;
                default:
    				if(prefix.Length > 0)
    					prefix.Remove (prefix.Length - 1);
    				break;
            }
        }

        return (prefix.Length > 0 ? prefix + " " + coffeeType + suffix : "Black Coffee");
    }

    public void reset()
    {
        flavorCount = 0;

        savoryValue = 0;
        fishValue = 0;
        meatyValue = 0;
        bitterValue = 0;
        nuttyValue = 0;
        fruityValue = 0;
        sweetValue = 0;
        saltyValue = 0;
        sourValue = 0;
        floweryValue = 0;
        cleanValue = 0;
        spicyValue = 0;
        creamyValue = 0;
        aromaticValue = 0;
        horribleValue = 0;

        for (int i = 0; i < flavors.Length; i++)
            flavors[i] = Flavor.NONE;
    }
}
