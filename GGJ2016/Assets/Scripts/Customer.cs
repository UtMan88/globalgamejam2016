﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Customer : MonoBehaviour
{
	public Text requestText = null;
    public int savory = 0;
    public int fish = 0;
    public int meaty = 0;
    public int bitter = 0;
    public int nutty = 0;
    public int fruity = 0;
    public int sweet = 0;
    public int salty = 0;
    public int sour = 0;
    public int flowery = 0;
    public int clean = 0;
    public int spicy = 0;
    public int creamy = 0;
	public int aromatic = 0;
	public int horrible = 0;

    public int score = 0;
	public int min = 1;
	public int max = 10;
    public float timer = 0;

	void Awake()
	{
		GameObject go = GameObject.Find ("CustomerText");
		if (go != null) {
			requestText = go.GetComponent<Text> ();
		} else {
			Debug.LogError ("Couldn't find the customer text.");
		}
	
	}
    void Start()
    {
        timer = Random.Range(45, 90);

		int horribleSelector = Random.Range(0, 10);
		int flavorSelector = Random.Range (0, 10);
		if (horribleSelector == 5) {
			horrible = 10;
			fish    = Random.Range(min, max);
		}
		switch (flavorSelector) {

		case 1:
			savory  = Random.Range(min, max);
			salty   = Random.Range(min, max);
		break;
		case 2:
			flowery = Random.Range (min, max);
			aromatic = Random.Range (min, max);
			break;
		case 3:
			sweet   = Random.Range(min, max);
			fruity  = Random.Range(min, max);
			creamy =  Random.Range(min, max);
			break;
		case 4:
			savory  = Random.Range(max/2, max);
			bitter  = Random.Range(min, max);
			nutty   = Random.Range(min, max);
			break;
		case 5:
			savory  = Random.Range(min, max);
			meaty   = Random.Range(min, max);
			salty = Random.Range(min, max);
			break;
		case 6:
			bitter  = Random.Range(min, max);
			aromatic = Random.Range (min, max);
			break;
		case 7:
			clean   = Random.Range(max/2, max);
			sweet   = Random.Range(min, max);
			break;
		case 8: 
			savory = Random.Range (min, max);
			spicy =  Random.Range(min, max);
			break;
		case 9:
			sour  = Random.Range(min, max);
			creamy =  Random.Range(min, max);
			break;
		default:
			break;
		}

    
		requestText.text = getRequest ();
	}

	string getRequest()
	{
		string request = "I'm looking for... \n";
		int total = savory + fish + meaty + bitter + nutty + fruity + sweet + flowery + creamy + spicy + clean + aromatic + horrible + salty + sour;
		if (total == 0) {
			request = "A damned fine cup of coffee.";
		} else {
			if (savory > 0) {
				request += "Something bold... \n";
			}
			if (fish > 0) {
				request += "Something a bit different... \n";
			}
			if (meaty > 0) {
				request += "Something that reminds me of home... \n";
			}
			if (bitter > 0) {
				request += "Something with a unique mouth feel... \n";
			}
			if (nutty > 0) {
				request += "Something with a nutty top note... \n";
			}
			if (fruity > 0) {
				request += "Something with natural sweetness... \n";
			}
			if (sweet > 0) {
				request += "Something with a playful finish... \n";
			}
			if (salty > 0) {
				request += "Something salty... \n";
			}
			if (sour > 0) {
				request += "Something tart... \n";
			}
			if (flowery > 0) {
				request += "Something with a fresh scent... \n";
			}
			if (clean > 0) {
				request += "Something crisp... \n";
			}
			if (spicy > 0) {
				request += "Something with a bit of zip... \n";
			}
			if (creamy > 0) {
				request += "Something smooth... \n";
			}
			if (aromatic > 0) {
				request += "Something with a good aroma... \n";
			}
			if (fish > 0) {
				request += "Something reminiscent of the high seas... \n";
			}
			if (horrible > 0) {
				request += "Something TIAMAT would approve of... \n";
			}
		}

		return request; 
	}
    void Update()
    {
        timer -= Time.deltaTime;
    }
    public void calculateScore(MugContents mugContents)
    {
        score =     
            Mathf.Abs(savory - mugContents.savoryValue) +
            Mathf.Abs(fish - mugContents.fishValue) +
            Mathf.Abs(meaty - mugContents.meatyValue) +
            Mathf.Abs(bitter - mugContents.bitterValue) +
            Mathf.Abs(nutty - mugContents.nuttyValue) +
            Mathf.Abs(fruity - mugContents.fruityValue) +
            Mathf.Abs(sweet - mugContents.sweetValue) +
            Mathf.Abs(salty - mugContents.saltyValue) +
            Mathf.Abs(sour - mugContents.sourValue) +
            Mathf.Abs(flowery - mugContents.floweryValue) +
            Mathf.Abs(clean - mugContents.cleanValue) +
            Mathf.Abs(spicy - mugContents.spicyValue) +
            Mathf.Abs(creamy - mugContents.creamyValue);
        Debug.Log(score);
        GameState.AddCustomerScore(score);
        Debug.Log(timer);
    }
}
