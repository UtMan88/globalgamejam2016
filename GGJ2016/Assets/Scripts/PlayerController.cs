﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public enum SwipeDirection {
    Up,
    Down,
    Right,
    Left
}

public class PlayerController : MonoBehaviour
{
   
    public Movement movement;
    public Vector3 respawnPoint;
    public Texture2D pauseImage;

    [Header("Swipe Values")]
    public float minSwipeDistance = 50.0f;
    public float maxSwipeTime = 0.3f;

    private Touch touch;
    private Vector2 touchDirection;
    private Vector2 touchStart;
    private Vector2 touchEnd;
    private float touchStartTime = 0.0f;
    private SwipeDirection swipeDirection;
    private bool isNewTouch = false;

    void Start()
    {
        Respawn();
    }
    void Update ()
    {
        // Get flicks if using touch-supported device
        if (Input.touchCount > 0) {
            touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began) {
                touchStart = touch.position;
                touchStartTime = Time.time;
            }
            if (touch.phase == TouchPhase.Ended || touch.phase == TouchPhase.Canceled) {
                float swipeTime = 0.0f;
                float swipeDistance = 0.0f;
                touchEnd = touch.position;
                touchDirection = touchEnd - touchStart;
                swipeTime = Time.time - touchStartTime;
                swipeDistance = touchDirection.magnitude;

                if ((swipeTime < maxSwipeTime) && (swipeDistance > minSwipeDistance)) {
                    if (Mathf.Abs(touchDirection.x) > Mathf.Abs(touchDirection.y)) {
                        if (touchDirection.x > 0) {
                            swipeDirection = SwipeDirection.Right;
                        } else {
                            swipeDirection = SwipeDirection.Left;
                        }
                    } else {
                        if (touchDirection.y > 0) {
                            swipeDirection = SwipeDirection.Up;
                        } else {
                            swipeDirection = SwipeDirection.Down;
                        }
                    }

                    isNewTouch = true;
                }
            }
        }

        if (Input.GetKeyDown("p"))
        {
            GameState.pause = !GameState.pause;
        }
        if (!GameState.pause)
        {
            if (Input.GetKey("w") || (isNewTouch && (swipeDirection == SwipeDirection.Up))) {
                movement.Move("up");
                if (isNewTouch) {
                    isNewTouch = false;
                }
            }
            if (Input.GetKey("a") || (isNewTouch && (swipeDirection == SwipeDirection.Left))) {
                movement.Move("left");
                if (isNewTouch) {
                    isNewTouch = false;
                }
            }
            if (Input.GetKey("d") || (isNewTouch && (swipeDirection == SwipeDirection.Right))) {
                movement.Move("right");
                if (isNewTouch) {
                    isNewTouch = false;
                }
            }
            if (Input.GetKey("s") || (isNewTouch && (swipeDirection == SwipeDirection.Down))) {
                movement.Move("down");
                if (isNewTouch) {
                    isNewTouch = false;
                }
            }
        }
    }
    void OnGUI()
    {
        if (GameState.pause)
            GUI.Label(new Rect((Screen.width/4), (Screen.height/4), Screen.width/2, Screen.height/2), pauseImage);
    }

    public void Respawn()
    {
        Debug.Log("REEEEEEEEEEESPAWN");
        if (movement.type == Movement.MovementType.Translate)
        {
            transform.position = new Vector3(0, 0, 0);
        }
        else if(movement.type == Movement.MovementType.Slide)
        {
            if(movement.currentMove != null)
            {
                movement.currentMove.Complete();
                transform.position = new Vector3(0, 0, 0);
            }
        }
        gameObject.GetComponent<MugContents>().reset();
    }
}
