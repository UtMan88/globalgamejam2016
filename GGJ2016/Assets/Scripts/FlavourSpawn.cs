﻿using UnityEngine;
using System.Collections;

public class FlavourSpawn : MonoBehaviour
{

    public float flavourSpawnDelay = 3;
    Vector3 spawnPoint = new Vector3();
    public bool reversed = false;
    float timer = 0;
    public GameObject[] flavors = new GameObject[0];

	void Start ()
    {
        spawnPoint.z = -1;
        spawnPoint.y = transform.position.y;
        if (reversed)
            spawnPoint.x = -5;
        else
            spawnPoint.x = 5;
	}
    void Update()
    {
        timer += Time.deltaTime;
        if (!GameState.pause && timer > flavourSpawnDelay)
        {
            SpawnFlavour();
            timer = 0;
        }
    }
    void SpawnFlavour()
    {
        Instantiate(flavors[Random.Range(0,flavors.Length)], spawnPoint, transform.rotation);
    }
    public void SpawnFlavour(Vector3 position)
    {
        Instantiate(flavors[Random.Range(0, flavors.Length)], position, transform.rotation);
    }
}
