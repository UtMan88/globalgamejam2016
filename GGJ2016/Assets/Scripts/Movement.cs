﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class Movement : MonoBehaviour
{
    public enum MovementType
    {
        Translate,
        Slide
    };

    public MovementType type = MovementType.Translate;
    public Tween currentMove = null;

	public void Move (string direction)
    {
        switch (direction)
        {
            case "up": DoMovement(Vector3.up); break;
            case "left": DoMovement(Vector3.left); break;
            case "right": DoMovement(Vector3.right); break;
            case "down": DoMovement(Vector3.down); break;
        }
    }

    private void DoMovement(Vector3 direction)
    {
        switch(type)
        {
            case MovementType.Translate:
                {
                    transform.Translate(direction);
                }
                break;
            case MovementType.Slide:
                {
                    if(currentMove == null || currentMove.IsPlaying() == false)
                        currentMove = transform.DOMove(this.transform.position + direction, 0.1f);
                }
                break;
        }
    }
}
