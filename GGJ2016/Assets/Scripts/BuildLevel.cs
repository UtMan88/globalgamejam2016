﻿using UnityEngine;
using System.Collections;

public class BuildLevel : MonoBehaviour
{
    public GameObject conveyor;
    public GameObject customer;
    public int localDifficulty = 0;
    void Start ()
    {
        GameObject cust = Instantiate(customer, transform.position, transform.rotation) as GameObject;
        GameState.currentCustomer = cust;

        localDifficulty = GameState.difficulty; 

        Instantiate(conveyor, new Vector3(0, 4, -1), transform.rotation);

        if (Random.Range(0, localDifficulty+4) < localDifficulty)
            Instantiate(conveyor, new Vector3(0, 1,-1), transform.rotation);
        if (Random.Range(0, 6) < localDifficulty + 1)
            Instantiate(conveyor, new Vector3(0, 2, -1), transform.rotation);
        if (Random.Range(0, 6) < localDifficulty)
            Instantiate(conveyor, new Vector3(0, 3, -1), transform.rotation);
        if (Random.Range(0, 6) < localDifficulty +1)
            Instantiate(conveyor, new Vector3(0, 5, -1), transform.rotation);
        if (Random.Range(0, localDifficulty+4) < localDifficulty + 1)
            Instantiate(conveyor, new Vector3(0, 6, -1), transform.rotation);

    }
}
