﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{
    void Start()
    {
        GameState.reset();
    }
    void OnGUI()
    {
        if (GUI.Button(new Rect(0, 0, 100, 100), "Start"))
            SceneManager.LoadScene("mainmenu");
    }
}
