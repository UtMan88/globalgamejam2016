﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Timer : MonoBehaviour {

	public float currentTime = 30f;
	public Text timeText = null;

    public AudioSource bellSource;
    bool isPlayed = false;
	//set the time
	public void setTimer(float newTime)
	{
		currentTime = newTime;
	}

	void Awake()
	{
		timeText = GetComponent<Text> ();
	}
	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () 
	{
		currentTime = Mathf.Max(0,currentTime - Time.deltaTime);
		timeText.text = Mathf.Floor (currentTime).ToString ();
        if(isPlayed == false && currentTime <= 0)
        {
            isPlayed = true;
            bellSource.Play();
        }
	}
}
