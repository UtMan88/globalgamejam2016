﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class Sliders : MonoBehaviour {
    [System.Serializable]
    public class SliderPair
    {
        public Slider slider;
        public float value = 0;
    }

	[System.Serializable] 
	public class SliderContainer {
		public string name;
        public SliderPair CupSlider;
        public SliderPair CustSlider;
    }

	public Text Title;
	public SliderContainer[] sliders;
    public float preTime = 0.5f; // Time it waits before incrementing sliders
	public float rateOfProgress = 5f; 

	// Use this for initialization
	void OnEnable () {
		StartCoroutine ("incrementProgress");
	}
		
	public void initSliders(MugContents mc, Customer cust)
	{
		Title.text = mc.ToCoffeeString ();

        float maxVal = mc.savoryValue + mc.fishValue + mc.cleanValue + mc.meatyValue +
            mc.sweetValue + mc.spicyValue + mc.floweryValue + mc.saltyValue + mc.bitterValue +
            mc.nuttyValue + mc.fruityValue + mc.sourValue + mc.horribleValue + mc.aromaticValue;

        float maxValCust = cust.savory + cust.fish + cust.clean + cust.meaty +
            cust.sweet + cust.spicy + cust.flowery + cust.salty + cust.bitter +
            cust.nutty + cust.fruity + cust.sour + cust.horrible + cust.aromatic;

        foreach (SliderContainer sc in sliders)
		{
            sc.CupSlider.slider.maxValue = maxVal;
            sc.CustSlider.slider.maxValue = maxValCust;
            switch(sc.name)
            {
                case "Savory":
                    Debug.Log(mc.savoryValue + " vs " + cust.savory);
                    
                    sc.CupSlider.value = mc.savoryValue;
                    sc.CustSlider.value = cust.savory;
                    break;
                case "Fishy":
                    sc.CupSlider.value = mc.fishValue;
                    sc.CustSlider.value = cust.fish;
                    break;
                case "Clean":
                    sc.CupSlider.value = mc.cleanValue;
                    sc.CustSlider.value = cust.clean;
                    break;
                case "Meaty":
                    sc.CupSlider.value = mc.meatyValue;
                    sc.CustSlider.value = cust.meaty;
                    break;
                case "Sweet":
                    sc.CupSlider.value = mc.sweetValue;
                    sc.CustSlider.value = cust.sweet;
                    break;
                case "Spicy":
                    sc.CupSlider.value = mc.spicyValue;
                    sc.CustSlider.value = cust.spicy;
                    break;
                case "Flowery":
                    sc.CupSlider.value = mc.floweryValue;
                    sc.CustSlider.value = cust.flowery;
                    break;
                case "Salty":
                    sc.CupSlider.value = mc.saltyValue;
                    sc.CustSlider.value = cust.salty;
                    break;
                case "Bitter":
                    sc.CupSlider.value = mc.bitterValue;
                    sc.CustSlider.value = cust.bitter;
                    break;
                case "Nutty":
                    sc.CupSlider.value = mc.nuttyValue;
                    sc.CustSlider.value = cust.nutty;
                    break;
                case "Fruity":
                    sc.CupSlider.value = mc.fruityValue;
                    sc.CustSlider.value = cust.fruity;
                    break;
                case "Sour":
                    sc.CupSlider.value = mc.sourValue;
                    sc.CustSlider.value = cust.sour;
                    break;
                case "Horrible":
                    sc.CupSlider.value = mc.horribleValue;
                    sc.CustSlider.value = cust.horrible;
                    break;
                case "Aromatic":
                    sc.CupSlider.value = mc.aromaticValue;
                    sc.CustSlider.value = cust.aromatic;
                    break;
                case "Creamy":
                    sc.CupSlider.value = mc.creamyValue;
                    sc.CustSlider.value = cust.creamy;
                    break;
                default:
                    Debug.LogWarning("Umm, what is this '" + sc.name + "' you speak of, stranger?");
                    sc.CupSlider.value = 0;
                    sc.CustSlider.value = 0;
                    break;
            }
            /*
			if (sc.name == "Savory") {
				Debug.Log ( mc.savoryValue);
				sc.slider.maxValue = mc.savoryValue;
			} else if (sc.name == "Fishy") {
				sc.slider.maxValue = mc.fishValue;
			} else if (sc.name == "Clean") {
				sc.slider.maxValue = mc.cleanValue;
			} else if (sc.name == "Meaty") {
				sc.slider.maxValue = mc.meatyValue;
			} else if (sc.name == "Sweet") {
				sc.slider.maxValue = mc.sweetValue;
			} else if (sc.name == "Spicy") {
				sc.slider.maxValue = mc.spicyValue;
			} else if (sc.name == "Flowery") {
				sc.slider.maxValue = mc.floweryValue;
			} else if (sc.name == "Salty") {
				sc.slider.maxValue = mc.saltyValue;
			} else if (sc.name == "Bitter") {
				sc.slider.maxValue = mc.bitterValue;
			} else if (sc.name == "Nutty") {
				sc.slider.maxValue = mc.nuttyValue;
			} else if (sc.name == "Fruity") {
				sc.slider.maxValue = mc.fruityValue;
			} else if (sc.name == "Sour") {
				sc.slider.maxValue = mc.sourValue;
			} 
			else if (sc.name == "Horrible") {
				sc.slider.maxValue = mc.horribleValue;
			}
			else if (sc.name == "Aromatic") {
				sc.slider.maxValue = mc.aromaticValue;
			}
			else {
				sc.slider.maxValue = 0;
			}
            */

        }

	}

	IEnumerator incrementProgress()
	{
		yield return new WaitForSeconds (preTime); 
		foreach (SliderContainer sc in sliders) 
		{
            Debug.Log("Adjusting slider for " + sc.name + " ( Cup: " + sc.CupSlider.value + " Cust: " + sc.CustSlider.value + ")");
			while (sc.CupSlider.value - sc.CupSlider.slider.value >= 0.1f)
            {
                //sc.slider.value = Mathf.Lerp (sc.slider.value, sc.slider.maxValue, Time.deltaTime * rateOfProgress);
                sc.CupSlider.slider.value = Mathf.Lerp(sc.CupSlider.slider.value, sc.CupSlider.value, Time.deltaTime * rateOfProgress);
                yield return null;
            }

            while (sc.CustSlider.value - sc.CustSlider.slider.value >= 0.1f)
            {
                sc.CustSlider.slider.value = Mathf.Lerp(sc.CustSlider.slider.value, sc.CustSlider.value, Time.deltaTime * rateOfProgress);
                yield return null;
            }

        }
	}
}
