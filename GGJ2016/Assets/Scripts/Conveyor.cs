﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Conveyor : MonoBehaviour
{
    float timer = 0f;
    public float conveyorDelay = 1f;
    public bool prewarm = false;
    List<GameObject> stuff = new List<GameObject>();
    string direction = "left";
    FlavourSpawn flavorSpawn; // Murica.

    void Start()
    {
        flavorSpawn = gameObject.GetComponent<FlavourSpawn>();
        if (Random.Range(0, 3) < 1)
        {
            flavorSpawn.reversed = true;
            direction = "right";
        }
        int slowestSpeed = 4;

        slowestSpeed -= GameState.difficulty;
        if (slowestSpeed < 1)
            slowestSpeed = 1;
        conveyorDelay = Random.Range(1, slowestSpeed);

        // Prewarm
        if(prewarm)
        {
            float start;
            float step;
            float end;
            if(flavorSpawn.reversed)
            {
                start = -5;
                step = 1;
                end = 5;
            }
            else
            {
                start = 5;
                step = -1;
                end = -5;
            }
            for(float i = start; i != end; i += step)
            {
                // Random skip
                if (Random.value >= 0.5f)
                    continue;
                flavorSpawn.SpawnFlavour(new Vector3(i, transform.position.y, -1));
            }
        }
    }
    void Update()
    {
        if (!GameState.pause)
        {
            timer += Time.deltaTime;
            if (timer >= conveyorDelay)
            {
                timer = 0;
                foreach (GameObject moveMe in stuff)
                {
                    if (moveMe == null)
                    {
                        stuff.Remove(moveMe);
                        break;
                    }
                    else
                        moveMe.GetComponent<Movement>().Move(direction);
                }
            }
        }
    }
    public void Remove(GameObject toBeRemoved)
    {
        if (stuff.Contains(toBeRemoved))
            stuff.Remove(toBeRemoved);
    }
    void OnTriggerEnter2D(Collider2D MoveCheck)
    {
        if (MoveCheck.GetComponent<Movement>())
            stuff.Add(MoveCheck.gameObject);  
    }
    void OnTriggerExit2D(Collider2D MoveCheck)
    {
        if (MoveCheck.GetComponent<Movement>())
            if (stuff.Contains(MoveCheck.gameObject))
                stuff.Remove(MoveCheck.gameObject);
    }
}
