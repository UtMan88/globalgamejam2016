﻿using UnityEngine;
using System.Collections;

public class PlayOnEnable : MonoBehaviour
{

    public AudioSource clip;

    void Awake()
    {
        if(clip == null)
        {
            clip = GetComponent<AudioSource>();
        }
    }

    void OnEnable()
    {
        if(clip != null)
        {
            clip.Play();
        }
    }
}
