﻿using UnityEngine;
using System.Collections;

public enum Flavor
{
    NONE = 0,
    LEATHER,
    LAVENDER,
    HONEY,
    LEMON,
    PEPPER,
    JALAPENO,
    BEES,
    PINE,
    CHOCOLATE,
    BANANA,
    RAISINS,
    VANILLA,
    CINNAMON,
    SOAP,
    CREAM,
    TANGERINE,
    PEANUT,
    ALMONDS,
    HAZELNUTS,
    CARAMEL,
    SANDALS,
    COCONUT,
    HONEYDEW,
    MINT,
    PUMPKIN,
    STRAWBERRY,
    TOFFEE,
    ROSES,
    JASMINE,
    CHERRIES,
	FISH,
    SIRRACHA,
    MILK
}

public class FlavorItem : MonoBehaviour
{

    public Flavor flavor = Flavor.NONE;
    public AudioSource playOnPickup;
    public AudioClip audio;

    [Header("Taste Values")]
    public int savory = 0;
    public int fish = 0;
    public int meaty = 0;
    public int bitter = 0;
    public int nutty = 0;
    public int fruity = 0;
    public int sweet = 0;
    public int salty = 0;
    public int sour = 0;
    public int flowery = 0;
    public int clean = 0;
    public int spicy = 0;
    public int creamy = 0;
	public int horrible = 0;
	public int aromatic = 0;
    
    void Start()
    {
        GameObject go = GameObject.Find("SoundPlayer");
        playOnPickup = go.GetComponent<AudioSource>();
    }
    
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "Player")
        {
            if(playOnPickup != null)
            {
                playOnPickup.clip = audio;
                playOnPickup.Play();
            }
            collider.gameObject.GetComponent<MugContents>().addIngredients(this);

            Destroy(gameObject);
        }
    }
}
