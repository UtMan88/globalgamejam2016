﻿using UnityEngine;
using System.Collections;

public class LevelEdge : MonoBehaviour {
	public AudioSource playOnFall;
	public AudioClip glassclip;
	void Start()
	{
		GameObject go = GameObject.Find("SoundPlayer");
		playOnFall = go.GetComponent<AudioSource>();
	}

    void OnTriggerEnter2D(Collider2D playerCheck)
    {
        PlayerController pc = playerCheck.GetComponent<PlayerController>();
        Debug.Log("Fall off the handle, " + pc);
		if (pc != null) {
			pc.Respawn ();
			if(playOnFall != null)
			{
				playOnFall.clip = glassclip;
				playOnFall.Play();
			}
		}
        if (playerCheck.GetComponent<FlavorItem>())
            Destroy(playerCheck.gameObject);
	}
}
