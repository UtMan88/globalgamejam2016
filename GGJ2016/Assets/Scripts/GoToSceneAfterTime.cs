﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class GoToSceneAfterTime : MonoBehaviour {
    public string scene;
    public float time;
	// Use this for initialization
	void Start () {
        StartCoroutine("KillMe");
	}
	
	IEnumerator KillMe()
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene(scene);
    }
}
